#!/usr/pkg/bin/bash
source ./ytsrc
parse_formats() {
	out="$(~/bin/youtube-dl --default-search ytsearch1: "${1}" --list-formats)"
	formats=""
	if grep -q "opus @ 50k" <<< "$out"; then
		echo "webm (249) found"
		formats="$formats 249"
		fi
	if grep -q "opus @ 70k" <<< "$out"; then
		echo "webm (250) found"
		formats="$formats 250"
		fi
	if grep -q "mp4a.40.5@ 48k" <<< "$out"; then
		echo "m4a (139) found"
		formats="$formats 139"
		fi
	if grep -q "mp4a.40.2@128k" <<< "$out"; then
		echo "m4a (140) found"
		formats="$formats 140"
		fi
}


#LOOKUP=test
echo -n 'enter search term ' >&2
read LOOKUP
echo "Searching for $LOOKUP"
out=$(~/bin/youtube-dl --default-search ytsearch1: "$LOOKUP" -e)
echo "Found $out"

while true; do
read -p "Do you wish to continue? " yn
case $yn in
	[Yy]* ) echo "running search for formats";
		parse_formats "${LOOKUP}";
		echo "Formats found are $formats"
		select name in $formats
			do
			if [ $name ] ; then
				FORMAT=$name
				break
			else 
				echo 'invalid selection.'
			fi
			done
		echo -n 'enter filename: '
		read FILENAME
		case $FORMAT in 
		249|250 )
			~/bin/youtube-dl --default-search ytsearch1: "${LOOKUP}" -f $FORMAT -o ${BASEDIR}${FILENAME}.webm;
			break;;
		139|140 )
			~/bin/youtube-dl --default-search ytsearch1: "${LOOKUP}" -f $FORMAT -o ${BASEDIR}${FILENAME}.m4a;;
			esac
			
	break;;
	[Nn]* ) echo "Exiting.."; exit;;
	* ) echo "invalid selection";; 
	esac
	done
					
					
